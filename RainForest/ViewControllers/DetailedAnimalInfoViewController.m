//
//  DetailedAnimalInfoViewController.m
//  RainForestStarter
//
//  Created by Volodymyr Viniarskyi on 3/15/18.
//  Copyright © 2018 Razeware LLC. All rights reserved.
//

#import "DetailedAnimalInfoViewController.h"
#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface DetailedAnimalInfoViewController()
@end

@implementation DetailedAnimalInfoViewController
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
  [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
  [self.node.collectionNode.view.collectionViewLayout invalidateLayout];
}

@end
