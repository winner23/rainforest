//
//  DetailedCellNode.h
//  RainForest
//
//  Created by Volodymyr Viniarskyi on 3/21/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface DetailedCellNode : ASCellNode
@property (nonatomic, assign) NSInteger row;
@property (nonatomic, copy) NSString *imageCategory;
@property (nonatomic, strong) ASNetworkImageNode *imageNode;
@end
