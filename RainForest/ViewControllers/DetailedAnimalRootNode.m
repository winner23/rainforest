//
//  DetailedAnimalRootNode.m
//  RainForest
//
//  Created by Volodymyr Viniarskyi on 3/20/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "DetailedAnimalRootNode.h"
#import "RainforestCardInfo.h"
#import "DetailedCellNode.h"
#import <Alamofire/Alamofire-Swift.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface DetailedAnimalRootNode()
@property (copy, nonatomic) NSString *cardInfo;
@property (strong, nonatomic) ASCollectionNode *collectionNode;
@property (strong, nonatomic) NSArray *urlImages;
@property (assign, nonatomic) BOOL update;
@end

@interface DetailedAnimalRootNode(ASCollectionDelegate)<ASCollectionDelegate>
@end

@interface DetailedAnimalRootNode(ASCollectionDataSource)<ASCollectionDataSource>
@end


@implementation DetailedAnimalRootNode
static const NSString *KEY = @"8384288-0bf900ed1e45464c05f70d4fb";
static const NSInteger kImageHeight = 220;

- (instancetype)initWithAnimal:(NSString *) cardInfo {
  self = [super init];
  if (self) {
    self.automaticallyManagesSubnodes = YES;
    _cardInfo = cardInfo;
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    _collectionNode = [[ASCollectionNode alloc] initWithCollectionViewLayout:layout];
    _collectionNode.delegate = self;
    _collectionNode.dataSource = self;
    _collectionNode.backgroundColor = [UIColor greenColor];
    [self imagesURLPrepare];
  }
  return self;
}

#pragma mark - ASDisplayNode

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
  return [ASWrapperLayoutSpec wrapperWithLayoutElement:self.collectionNode];
}

- (NSURL *)requestURL {
  NSString *requestURL = [NSString stringWithFormat:@"https://pixabay.com/api/?key=%@&q=%@&image_type=photo&category=animals", KEY, self.cardInfo];
  NSURL *result = [NSURL URLWithString:[requestURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
  return result;
}
- (void)imagesURLPrepare {

  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[self requestURL]];
  [request setHTTPMethod:@"GET"];
  [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
  [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data,
                                                                                 NSURLResponse * _Nullable response,
                                                                                 NSError * _Nullable error) {
    NSMutableArray *result = [NSMutableArray new];
    NSError *err;
    if (error) {
      return;
    }
    if (data != nil) {
      NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                           options:NSJSONReadingMutableContainers
                                                             error:&err];
      if (json.count > 0) {
        NSInteger total = [json[@"total"] integerValue];
        NSInteger totalHits = [json[@"totalHits"] integerValue];
        NSArray *hits = json[@"hits"];
        
        NSLog(@"Total: %lul, TotalHits: %lul,", total, totalHits);
        if (total<1) {
          return;
        }
        for (NSDictionary *hit in hits) {
          NSString *url = hit[@"webformatURL"];
          [result addObject:url];
        }
        self.urlImages = result;
        dispatch_async(dispatch_get_main_queue(), ^{
          [self.collectionNode reloadData];
        });
      }
    }
    return;
  }] resume];
  return;
}

@end

#pragma mark - ASCollectionDataSource

@implementation DetailedAnimalRootNode(ASCollectionDataSource)
- (NSInteger)collectionNode:(ASCollectionNode *)collectionNode numberOfItemsInSection:(NSInteger)section {
  return self.urlImages.count;
}

- (ASCellNodeBlock)collectionNode:(ASCollectionNode *)collectionNode nodeBlockForItemAtIndexPath:(NSIndexPath *)indexPath {
  NSString *imageName = self.cardInfo;
  return ^{
    DetailedCellNode *cellNode = [DetailedCellNode new];
    cellNode.row = indexPath.row;
    cellNode.imageCategory = imageName;
    cellNode.imageNode.URL = [NSURL URLWithString:self.urlImages[indexPath.row]];
    return cellNode;
  };
}

- (ASSizeRange)collectionNode:(ASCollectionNode *)collectionNode constrainedSizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  CGSize imageSize = CGSizeMake(CGRectGetWidth(collectionNode.view.frame), kImageHeight);
  return ASSizeRangeMake(imageSize);
}
@end
