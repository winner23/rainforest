//
//  DetailedAnimalInfoViewController.h
//  RainForestStarter
//
//  Created by Volodymyr Viniarskyi on 3/15/18.
//  Copyright © 2018 Razeware LLC. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "DetailedAnimalRootNode.h"

@class RainforestCardInfo;
@interface DetailedAnimalInfoViewController : ASViewController<DetailedAnimalRootNode *>


@end
