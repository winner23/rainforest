//
//  DetailedAnimalRootNode.h
//  RainForest
//
//  Created by Volodymyr Viniarskyi on 3/20/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
@class RainforestCardInfo;

@interface DetailedAnimalRootNode : ASDisplayNode

@property (strong, nonatomic, readonly) ASCollectionNode *collectionNode;

- (instancetype)initWithAnimal:(NSString *) cardInfo;
@end
