//
//  DetailedCellNode.m
//  RainForest
//
//  Created by Volodymyr Viniarskyi on 3/21/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "DetailedCellNode.h"

@implementation DetailedCellNode

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.automaticallyManagesSubnodes = YES;
    _imageNode = [ASNetworkImageNode new];
    _imageNode.backgroundColor = ASDisplayNodeDefaultPlaceholderColor();
  }
  return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
  return [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0 child:self.imageNode];
}

- (void)layoutDidFinish {
  [super layoutDidFinish];
  //self.imageNode.URL = [self imageURL];
}

//- (NSURL *)imageURL {
//  CGSize imageSize = self.calculatedSize;
//  NSString *imageURLString = [NSString stringWithFormat:@"https://pixabay.com/api/?key=%@&q=%@&image_type=photo&per_page=%lul&min_width=%lul&min_height=%lul",
//                              KEY, self.imageCategory, (NSInteger) self.row, (NSInteger) imageSize.width, (NSInteger) imageSize.height];
//  return [NSURL URLWithString:imageURLString];
//  
//}
@end
