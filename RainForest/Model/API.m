//
//  API.m
//  RainForestStarter
//
//  Created by Volodymyr Viniarskyi on 3/16/18.
//  Copyright © 2018 Razeware LLC. All rights reserved.
//

#import "API.h"
#import <AFNetworking.h>
#import <AFURLSessionManager.h>

@implementation API

static NSString const *API_KEY = @"8384288-0bf900ed1e45464c05f70d4fb";

- (NSURLRequest *)getURLRequestForSearchString:(NSString *) searchString {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc]initWithSessionConfiguration:configuration];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    NSString *preparedURL = [NSString stringWithFormat:@"https://pixabay.com/api/?key=%@&q=%@&iamge_type=photo&category=animals",
                             API_KEY, searchString];
    NSURL *URL = [NSURL URLWithString:preparedURL];
    NSMutableURLRequest *reqest = [NSMutableURLRequest requestWithURL:URL];
    [reqest setHTTPMethod:@"GET"];
    
    return reqest;
}

- (void)collectURLSWithRequest:(NSURLRequest *)request {
    
}

@end
